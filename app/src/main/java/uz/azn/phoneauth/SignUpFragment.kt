package uz.azn.phoneauth

import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import uz.azn.phoneauth.databinding.FragmentSignUpBinding

class SignUpFragment : Fragment(R.layout.fragment_sign_up) {
    private lateinit var binding: FragmentSignUpBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSignUpBinding.bind(view)

        with(binding) {
            btnPhoneSignUp.setOnClickListener {
                val phoneNumber = etPhone.text.toString()
                if (phoneNumber.isNotEmpty()) {
                    val verificationFragment = VerificationFragment()
                    val bundle = Bundle()
                    bundle.putString("phone", phoneNumber)
                    verificationFragment.arguments = bundle
                    fragmentManager!!.beginTransaction()
                        .replace(R.id.frame_layout, verificationFragment).commit()
                }else{
                    Toast.makeText(
                        requireContext(),
                        "Telefonizni qaytatdan kiriting",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        }
    }

}