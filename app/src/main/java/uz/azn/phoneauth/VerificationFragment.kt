package uz.azn.phoneauth

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.arch.core.executor.TaskExecutor
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import uz.azn.phoneauth.databinding.FragmentVerificationBinding
import java.util.concurrent.TimeUnit

class VerificationFragment : Fragment(R.layout.fragment_verification) {
    private lateinit var binding: FragmentVerificationBinding
    var originalPhone = ""
    var verificationId = ""
    private lateinit var mAuth: FirebaseAuth
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentVerificationBinding.bind(view)
        mAuth = FirebaseAuth.getInstance()
        var formattedPhone = arguments!!.getString("phone")
        originalPhone = formattedPhone!!
        formattedPhone = formatPhone(originalPhone)
        startTimer()
        sendVerification(originalPhone)
        with(binding) {
            tvInfo.text = "Bir martalik kod $formattedPhone\n raqamiga yuborildi"
        etPhone.apply {
            addTextChangedListener(object :TextWatcher{
                override fun afterTextChanged(s: Editable?) {

                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (s?.length==6 && s==verificationId){
                        fragmentManager!!.beginTransaction().replace(R.id.frame_layout,MainFragment()).commit()
                    }
                }

            })
        }
        }
    }

    private fun formatPhone(number: String): String {
        try {
            return String.format(
                "%s%s%s%s (%s%s) %s%s%s-%s%s-%s%s",
                number[0],
                number[1],
                number[2],
                number[3],
                number[4],
                number[5],
                number[6],
                number[7],
                number[8],
                number[9],
                number[10],
                number[11],
                number[12]
            )
        } catch (ae: ArrayIndexOutOfBoundsException) {
            ae.printStackTrace()
        }
        return ""

    }

    private fun startTimer() {
        object : CountDownTimer(60000, 1000) {
            override fun onFinish() {
            }

            override fun onTick(millisUntilFinished: Long) {
                val mill = (millisUntilFinished / 1000).toInt()
                updateTimer(mill)
            }

        }.start()
    }

    private fun updateTimer(secondLeft: Int) {
        val minutes = secondLeft / 60
        val seconds = secondLeft/1000%60
        var secondStr = seconds.toString()
        var minuteStr = minutes.toString()

        if (secondStr == "0") {
            secondStr = "00"
        }
        if (secondStr.length == 1)
            secondStr = "0$secondStr"

        if (minuteStr.length == 1)
            secondStr = "0$minuteStr"

        binding.tvTimer.text = "$minuteStr:$secondStr"

    }
    fun sendVerification(phone:String){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phone,
            60,
            TimeUnit.SECONDS,
            requireActivity(),
            object :PhoneAuthProvider.OnVerificationStateChangedCallbacks(){
            override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                val code = p0.smsCode
                Log.d("code", "onVerificationCompleted: $code")
                verifyCode(code)

            }

            override fun onVerificationFailed(p0: FirebaseException) {
                Log.d("TAG", "${p0.message.toString()}")
            }
//  bu yuborilgan kodni olvoladi
                override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
                    verificationId = p0
                }

        })
        // sha sha-256 ni qoshish kerak fire basega android dan olib
    }
    fun verifyCode(code:String){
        val credential = PhoneAuthProvider.getCredential(verificationId,code)
        signInWithCredential(credential)
        
    }
  private  fun signInWithCredential(credential:PhoneAuthCredential){
mAuth.signInWithCredential(credential).addOnCompleteListener{
    task ->
    if (task.isSuccessful){
        fragmentManager!!.beginTransaction().replace(R.id.frame_layout, MainFragment())
        // bu qolgan fragment oynalrni yopadi yani bitta oyna qoladi
        fragmentManager!!.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }
    else
        Toast.makeText(requireContext(), task.exception!!.message.toString(), Toast.LENGTH_SHORT).show()
}

    }
}